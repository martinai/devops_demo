# ------- APPSERVER BUILD ------
FROM node:12

MAINTAINER Martin AI

# Environment Variables
ENV PORT=3000

WORKDIR /usr/src/demo
COPY . /usr/src/demo

RUN npm install

EXPOSE $PORT

ENTRYPOINT [ "npm", "start" ]
