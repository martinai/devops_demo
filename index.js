const express = require("express");
const app = express()
const message = process.env.MESSAGE

app.get("/", (req, res) => {
  if (message) {
    return res.send(message)
  }
  return res.send("no message found")
})

app.listen(3000, "0.0.0.0", () => {
  console.log("listening on port 3000")
})
